﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyCTRL : MonoBehaviour {

    [SerializeField] Transform[] wayPointsArray;
    [SerializeField] float chaseDistance = 15f;
    [SerializeField] float atkDistance = 5f;
    [SerializeField] float alertDistance = 2f;

    [SerializeField] Image statusPatrol;
    [SerializeField] Image statusAlert;
    [SerializeField] Image statusAttack;


    private int nextWP = 0;
    private Transform playerTrans;
    private NavMeshAgent nma;
    private enum CurrentState {patroling, alert, chasing, attacking, dead};
    private CurrentState curState;



	void Awake ()
    {
        playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
        nma = GetComponent<NavMeshAgent>();
        statusAlert.enabled = false;
        statusAttack.enabled = false;
        statusPatrol.enabled = false;
    }

    void Start ()
    {
        curState = CurrentState.patroling;
    }
	
	void Update ()
    {
        EnemyStatus();
	}

    void EnemyStatus ()
    {
        switch(curState)
        {
            case CurrentState.patroling:
                OnPatrol();
                break;
            case CurrentState.alert:
                OnAlert();
                break;
            case CurrentState.chasing:
                OnChase();
                break;
            case CurrentState.attacking:
                OnAttack();
                break;
            case CurrentState.dead:
                OnDead();
                break;
        }
    }

    void OnPatrol()
    {
        if (Vector3.Distance(transform.position, playerTrans.position) < chaseDistance + alertDistance)
        {
            curState = CurrentState.alert;
        }
        else if (Vector3.Distance (transform.position, playerTrans.position) < chaseDistance)
        {
            curState = CurrentState.chasing;
        }
        else if (Vector3.Distance (transform.position, playerTrans.position) < atkDistance)
        {
            curState = CurrentState.attacking;
        }
        else
        {
            // Deactivate other states' images
            // Activate Patrol image
            // Play Patrol animation
            nma.destination = wayPointsArray[nextWP].position;
            nma.Resume();
            if (nma.remainingDistance <= nma.stoppingDistance && !nma.pathPending)
            {
                if (nextWP < wayPointsArray.Length -1)
                    nextWP++;
                else
                    nextWP = 0;
            }
        }
    }

    void OnAlert ()
    {
        if (Vector3.Distance(transform.position, playerTrans.position) < chaseDistance)
        {
            curState = CurrentState.chasing;
        }
        else
        {
            // Deactivate other states' images
            // Activate Alert image
            // Play Alert animation
            // Stand still for 5 seconds
            curState = CurrentState.patroling;
         }
    }

    void OnChase ()
    {
        if (Vector3.Distance(transform.position, playerTrans.position) < atkDistance)
        {
            curState = CurrentState.attacking;
        }
        else if (Vector3.Distance(transform.position, playerTrans.position) < chaseDistance + alertDistance)
        {
            curState = CurrentState.alert;
        }
        else
        {
            // Deactivate other states' images
            // Activate Alert image
            // Play Alert animation
            // Chase player
            transform.LookAt(playerTrans);
            nma.destination = wayPointsArray[nextWP].position;
            nma.Resume();

        }

    }

    void OnAttack()
    {
        if (Vector3.Distance(transform.position, playerTrans.position) < chaseDistance)
        {
            curState = CurrentState.chasing;
        }
        else if (Vector3.Distance(transform.position, playerTrans.position) < chaseDistance + alertDistance)
        {
            curState = CurrentState.alert;
        }
        else
        {
            // Deactivate other states' images
            // Activate Attack image
            // Play Attack animation
            // Shoot player
        }

    }

    void OnDead()
    {
        // Deactivate All images
        // Play Death animation
        // Destroy after 3 seconds
    }
}
