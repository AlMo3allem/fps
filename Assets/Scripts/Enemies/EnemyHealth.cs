﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    [SerializeField] Canvas enemyCanvas;
    [SerializeField] Image healthBarBG;
    [SerializeField] Image healthBar;
    [SerializeField] Text healthText;

    private float enemyFullHealth = 100;
    private float enemyCurrentHealth;

    void Start ()
    {
        enemyCurrentHealth = enemyFullHealth;
        healthText.text = enemyCurrentHealth.ToString();
        HandleHealth(25f);
    }

    void LookAtCam ()
    {
        enemyCanvas.transform.LookAt(enemyCanvas.transform.position + Camera.main.transform.rotation * Vector3.forward,
            Camera.main.transform.rotation * Vector3.up);

    }
	
	void Update ()
    {
        LookAtCam();
	}

    public void HandleHealth(float damage)
    {
        enemyCurrentHealth -= damage;
        if (enemyCurrentHealth <= 0)
            KillEnemy();
        healthBar.fillAmount = (enemyCurrentHealth / enemyFullHealth);
        healthText.text = enemyCurrentHealth.ToString();
    }

    void KillEnemy()
    {
        // Play death animation
        // Destroy enemy object
    }
}
