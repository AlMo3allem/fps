﻿using UnityEngine;
using System.Collections;

public class Qazifa : MonoBehaviour {

	[SerializeField] GameObject enfejar;

    private GameObject insEnfejar;
    private ContactPoint tasadom;
    private Quaternion dawaran;

	void OnCollisionEnter (Collision col)
    {
        tasadom = col.contacts[0];
        dawaran = Quaternion.FromToRotation(Vector3.up, tasadom.normal);
        insEnfejar = Instantiate(enfejar, tasadom.point, dawaran) as GameObject;
        Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
