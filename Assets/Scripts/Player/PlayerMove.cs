﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

    [SerializeField] float forwardSpeed = 5f;
    [SerializeField] float jumpForce = 5f;
    [SerializeField] float mouseSense = 3f;
    private float hor;
    private float ver;
    private float rotHor;
    private float rotVer;
    private float verRange = 50f;
    private float verVelocity;

    private CharacterController charCont;

    private Vector3 speed;

	void Start ()
    {
        charCont = GetComponent<CharacterController>();
	}
	
	void Update ()
    {
        Move ();
	}

    void Move ()
    {
        // Player rotation.
        rotHor = Input.GetAxis("Mouse X") * mouseSense;
        rotVer -= Input.GetAxis("Mouse Y") * mouseSense;
        rotVer = Mathf.Clamp(rotVer, -verRange, verRange);
        transform.Rotate(0, rotHor * mouseSense, 0);
        Camera.main.transform.localRotation = Quaternion.Euler(rotVer, 0, 0);

        // Player movement.
        hor = Input.GetAxis("Horizontal") * forwardSpeed;
        ver = Input.GetAxis("Vertical") * forwardSpeed;
        verVelocity += Physics.gravity.y * Time.deltaTime;

        if (Input.GetButtonDown("Jump") && charCont.isGrounded)
        {
            verVelocity = jumpForce;
        }

        speed = new Vector3(hor, verVelocity, ver);
        speed = transform.rotation * speed;

        charCont.Move(speed * Time.deltaTime);
    }
}
