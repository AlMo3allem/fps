﻿using UnityEngine;
using System.Collections;

public class MissileShooting : MonoBehaviour {

	[SerializeField] float explosionRadius = 10f;
    [SerializeField] float explosionPower = 5000f;

    private Collider[] motasademat;

	void Start ()
    {
        motasademat = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach(var hit in motasademat)
        {
            if (hit.GetComponent<Rigidbody>())
                hit.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, transform.position, explosionRadius);
        }

        if (GetComponent<ParticleEmitter>())
            StartCoroutine(EmitionControl());
	}
	
    IEnumerator EmitionControl ()
    {
        GetComponent<ParticleEmitter>().emit = true;
        yield return new WaitForSeconds(0.5f);
        GetComponent<ParticleEmitter>().emit = false;
    }
}
