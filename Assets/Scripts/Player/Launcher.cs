﻿using UnityEngine;
using System.Collections;

public class Launcher : MonoBehaviour {

	[SerializeField] float speed = 20f;
    [SerializeField] GameObject sarou5;

    private GameObject instantiatedSarou5;
    private Vector3 targetMove;

	void Start ()
    {
        targetMove = new Vector3(0f, 0f, speed);
	}
	
	
	void Update ()
    {
	if(Input.GetButtonDown("Fire1"))
        {
            instantiatedSarou5 = Instantiate(sarou5, transform.position, transform.rotation) as GameObject;
            instantiatedSarou5.GetComponent<Rigidbody>().velocity = transform.TransformDirection(targetMove);
            Physics.IgnoreCollision(instantiatedSarou5.GetComponent<Collider>(), transform.root.GetComponent<Collider>());
        }
	}
}
